# PHP Collections

In PHP we often use simple _arrays_ to contain our values. These arrays
are not typed and often we end up with mixed types, leading to confusion
later when we're looping over the values.

Collections are objects that can be typed, requiring that all values are
declared, allowing you to type-hint against that precise type in your
code.

Collections also provide additional functionality to assist you if you
need to iterate over the values, find the first of a specific type,
filter certain values out etc.

This package provides a base `Collection` which you can use on its own,
or you can extend it to implement a specific type collection.

I've also provided a helpful `collect()` function which you can use in
place of a typical class constructor. So instead of:

```php
$collection = new Antnee\Collection\Collection([
    'Something',
    'Something',
    'Something',
    'Dark side,
]);
```

You can simply use this:
 
```php
$collection = collect([
    'Something',
    'Something',
    'Something',
    'Dark side,
]);
``` 

Check out the tests to see some examples of how to use this class.

_(More tests are incoming)_