<?php
namespace Antnee\Collection;

class CollectionFilterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider filterEvenNumbersProvider
     * @param \array $expected
     * @param \int[] ...$integers
     */
    public function testFilterEvenNumbersByCallback(array $expected, int ...$integers)
    {
        $collection = (new Collection(...$integers))->filter(function ($val)
        {
            if ($val % 2 === 0) {
                return $val;
            }
        });
        $this->assertEquals($expected, $collection->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider filterNumbersAndFalseProvider
     * @param \array $expected
     * @param \int[]|\float[] ...$numbers
     */
    public function testFilterFalseValues(array $expected, ...$numbers)
    {
        $collection = (new Collection(...$numbers))->filter();
        $this->assertEquals($expected, $collection->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider filterNumbersAndFalseProvider
     * @param \array $expected
     * @param \int[]|\float[] ...$numbers
     */
    public function testFilterNonIntegerValues(array $expected, ...$numbers)
    {
        $collection = (new Collection(...$numbers))->filter(function ($val)
        {
            if (is_int($val)) {
                return $val;
            }
        });
        $this->assertEquals($expected, $collection->getArrayCopy());
    }



    // PROVIDERS --------------------------------------------------------------

    public function filterEvenNumbersProvider()
    {
        return [
            [[2,4,6,8,10], 0, 1 , 2, 3, 4, 5, 6, 7, 8, 9, 10],
            [[432,6512,4856974,1235632,2131564], 432, 449, 6512, 4856974, 12745, 1235632, 2131564],
            [[-2,-6,-8,-10,-1232], -2, -6, -8, -5, -9, -10, -123, -1232],
        ];
    }

    public function filterNumbersAndFalseProvider()
    {
        return [
            [[1,2,3,4,5,6,7,8,9,10], false, 1, 2, 3, 4, 5, 6, 7, 8, 9, false, false, false, false, false, false, 10],
        ];
    }

    public function expectIntegersGotMixedProvider()
    {
        return [
            [[123456, PHP_INT_MAX], 'dsa', new class{}, 123456, 132456.32, 'dsda', PHP_INT_MAX],
        ];
    }
}