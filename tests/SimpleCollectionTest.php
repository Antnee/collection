<?php
namespace Antnee\Collection;

class SimpleCollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider integerProvider
     * @param \int[] ...$integers
     */
    public function testCreateCollectionsOfIntegers(int ...$integers)
    {
        $collection = new Collection(...$integers);
        $this->assertEquals($integers, $collection->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider floatProvider
     * @param \float[] ...$floats
     */
    public function testCreateCollectionsOfFloats(float ...$floats)
    {
        $collection = new Collection(...$floats);
        $this->assertEquals($floats, $collection->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider arrayProvider
     * @param \array[] ...$arrays
     */
    public function testCreateCollectionsOfArrays(Array ...$arrays)
    {
        $collection = new Collection(...$arrays);
        $this->assertEquals($arrays, $collection->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider objectProvider
     * @param \object[] ...$objects
     */
    public function testCreateCollectionsOfObjects(...$objects)
    {
        $collection = new Collection(...$objects);
        $this->assertEquals($objects, $collection->getArrayCopy());
    }



    // PROVIDERS --------------------------------------------------------------

    public function integerProvider()
    {
        return [
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            [2, 4, 6, 8, 10, 12],
            [543, 76765, 876343, 13832, 11920112],
            [-13231434, -3241, -653465, -405403005],
            [PHP_INT_MIN, PHP_INT_MAX],
        ];
    }

    public function floatProvider()
    {
        return [
            [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
            [0.123, 1.564, 456456.132, 134.315645133, 112435.53, 1234564.1245678],
            [-0.15245, -1231.64165, -1654584564.12341, -324546891234.954324],
        ];
    }

    public function arrayProvider()
    {
        return [
            [[], [], [], []],
            [[12,321,43214], [231,4324,43153], [4314,431412,431412]],
            [[231.231,321321.321,321345.4563], [432554.254,523455.7652,25657.542,65346254.56454]],
            [[2143,'dsfs',324.324], ['fdsf',43.32,4324], ['321','df4he','g5.123','123.123']],
        ];
    }

    public function objectProvider()
    {
        return [
            [new \stdClass(), new class{}, (object)[]],
        ];
    }
}