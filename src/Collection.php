<?php
namespace Antnee\Collection;

use ArrayObject;
use Exception;
use JsonSerializable;

class Collection extends ArrayObject implements JsonSerializable {

    const FILTER_USE_KEY = \ARRAY_FILTER_USE_KEY;
    const FILTER_USE_BOTH = \ARRAY_FILTER_USE_BOTH;

    public function __construct(...$items)
    {
        parent::__construct($this->arrayItems($items));
    }

    public function jsonSerialize()
    {
        return $this->getArrayCopy();
    }

    private function arrayItems($items)
    {
        if (is_array($items)) {
            return $items;
        } elseif ($items instanceof self) {
            return $items->getArrayCopy();
        } elseif ($items instanceof JsonSerializable) {
            return $items->jsonSerialize();
        }

        return (array)$items;
    }

    /**
     * Copy entries into a new collection
     *
     * @param string $collection The collection type to copy into
     * @return Collection
     * @throws Exception
     */
    public function into(string $collection) : Collection
    {
        if (!class_exists($collection)) {
            throw new Exception('Unknown class name "'.$collection."'");
        }
        if (!is_subclass_of($collection, Collection::class)) {
            throw new Exception('Class "'.$collection.'" is not a Collection type');
        }
        return new $collection(...$this->getArrayCopy());
    }

    /**
     * Map Function Against Collection and Return New Collection
     *
     * @param callable $callable May take up to two arguments: First is the array value, the second is the array key
     * @return self New collection of the same type
     */
    public function map(callable $callable, array $data=null) : self
    {
        $array = $data ?? $this->getArrayCopy();
        return new static(...array_map($callable, $array, array_keys($array)));
    }

    /**
     * Walk Over Collection Entities
     *
     * Does not return; use map() for that
     *
     * @param callable $callable
     */
    public function each(callable $callable)
    {
        $array = $this->getArrayCopy();
        array_walk($array, $callable);
    }

    /**
     * Reduce Collection by Callable
     *
     * @param callable $callable Requires two arguments; the first to carry from the previous iteration, and the second as the item
     * @param null $carry Initial value, or returned if array is empty
     * @return mixed Type depends on return value of $callable
     */
    public function reduce(callable $callable, $carry=null)
    {
        $array = $this->getArrayCopy();
        return array_reduce($array, $callable, $carry);
    }

    /**
     * Filter Collection By Callable
     *
     * @param callable|null $callable Callback for each iteration. If null will just filter empty values from array
     * @param int|null $flag Collection::FILTER_USE_KEY or Collection::FILTER_USE_BOTH
     * @return self
     */
    public function filter($callable=null, int $flag=null) : self
    {
        $array = $this->getArrayCopy();
        if ($callable && is_callable($callable)) {
            return new static(...array_filter($array, $callable, $flag));
        }
        return new static(...array_filter($array));
    }

    /**
     * Get First Entry From Collection
     *
     * @param callable|null $callable If provided will return the first value that this callback returns
     * @param mixed|null If no result is found, return this instead
     * @return mixed
     */
    public function first(callable $callable=null, $default=null)
    {
        if (!$callable) {
            $callable = function($item) {
                return $item;
            };
        }
        $data = array_filter($this->getArrayCopy());
        foreach ($data as $key => $item) {
            if ($callable($item, $key)) {
                return $item;
            }
        }
        return $default;
    }

    /**
     * Get Last Entry From Collection
     *
     * @param callable|null $callable If provided will return the last value that this callback returns
     * @param mixed|null If no result is found, return this instead
     * @return mixed
     */
    public function last(callable $callable=null, $default=null)
    {
        if (!$callable) {
            $array = array_filter($this->getArrayCopy());
            return empty($array) ? $default : end($array);
        }
        return $this->map($callable)->last(null, $default);
    }

    /**
     * Check if Collection Contains Value
     *
     * @param mixed|callable $check
     * @return bool
     */
    public function contains($check) : bool
    {
        if (is_callable($check)) {
            return (bool)$this->first($check);
        }
        return in_array($check, $this->getArrayCopy());
    }

    /**
     * Pop Entity Off Of The End Of The Collection
     *
     * @return mixed
     */
    public function pop()
    {
        $array  = $this->getArrayCopy();
        $popped = array_pop($array);
        $this->exchangeArray($array);
        return $popped;
    }

    /**
     * Push Entities On To The End Of The Collection
     *
     * @param array ...$vals
     */
    public function push(...$vals)
    {
        foreach ($vals as $val) {
            $this->append($val);
        }
    }

    /**
     * Get a New Collection With Another Collection Merged In
     *
     * Returns a new object which contains the original and new elements
     *
     * @param Collection $add
     * @return self
     */
    public function merge(Collection $add) : self
    {
        $clone = clone($this);
        foreach ($add as $newElement) {
            $clone[] = $newElement;
        }
        return $clone;
    }

    /**
     * Get a New Collection With Contents Sorted
     *
     * @param callable|null $callable
     * @return Collection
     */
    public function sort(callable $callable=null) : self
    {
        $data = $this->getArrayCopy();
        if ($callable) {
            usort($data, $callable);
        } else {
            sort($data);
        }
        return new static(...$data);
    }
}