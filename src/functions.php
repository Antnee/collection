<?php
use Antnee\Collection\Collection;

if (!function_exists('collect')) {
    function collect($items) : Collection
    {
        if (in_array(strtolower(gettype($items)), ['array', 'traversable'])) {
            return new Collection(...$items);
        }
        return new Collection($items);
    }
}

if (!function_exists('collectInto')) {
    function collectInto(string $collectionType, $items) : Collection
    {
        if (in_array(strtolower(gettype($items)), ['array', 'traversable'])) {
            return new $collectionType(...$items);
        }
        return new $collectionType($items);
    }
}